# Crypto Tracker

An application to track cryptocurrency prices (not in real-time).

## Getting started

### Install dependencies

To install dependencies run `npm i` or `yarn`

### Running a local instance

To run a local instance of Crypto-Tracker run the command `npm start`
