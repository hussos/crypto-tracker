import React from 'react';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {TableSortLabel} from "@material-ui/core";
import {Data, Order} from "../CryptoTable/CryptoTable";
import {Cryptocurrency} from "../../Models/Cryptocurrency";
import HeightIcon from '@material-ui/icons/Height';

interface EnhancedTableHeadProps {
    rows: Cryptocurrency[];
    order: Order;
    orderBy: keyof Data;
    setOrder: (order: Order) => void;
    setOrderBy: (orderBy: keyof Data) => void;
}

const EnhancedTableHead: React.FC<EnhancedTableHeadProps> = props => {

    const {orderBy, setOrder, order, setOrderBy, rows} = props;

    const rowSort = property => (
        order === 'asc'?
            rows.sort((a, b) => a[property] < b[property] ? -1 : 1) :
            rows.sort((a, b) => b[property] < a[property] ? -1 : 1)
    );

    const onRequestSort = (event: React.MouseEvent<unknown>, property: keyof Data) => {
        rowSort(property);
        setOrderBy(property);
        setOrder(order === 'asc' ? 'desc' : 'asc');
    };

    const createSortHandler = (property: keyof Data) => (event: React.MouseEvent<unknown>) => {
        onRequestSort(event, property);
    };

    return (

        <TableHead>
            <TableRow>
                <TableCell
                    key={"symbol"}
                >
                    {"Symbol"}
                </TableCell>
                <TableCell
                    key={"rank"}
                    sortDirection={orderBy === "rank" ? order : false}
                >
                    <TableSortLabel
                        IconComponent={HeightIcon}
                        active={orderBy === "rank"}
                        direction={orderBy === "rank" ? order : 'asc'}
                        onClick={createSortHandler("rank")}
                    >
                        {"Rank"}
                    </TableSortLabel>
                </TableCell>
                <TableCell
                    key={"price"}
                    sortDirection={orderBy === "price" ? order : false}
                >
                    <TableSortLabel
                        IconComponent={HeightIcon}
                        active={orderBy === "price"}
                        direction={orderBy === "price" ? order : 'asc'}
                        onClick={createSortHandler("price")}
                    >
                        {"Price"}
                    </TableSortLabel>
                </TableCell>
                <TableCell>
                    {"Remove"}
                </TableCell>
            </TableRow>
        </TableHead>
    );
};

export default EnhancedTableHead;
