import {makeStyles} from "@material-ui/core/styles";

export const useCryptoSelectStyles = makeStyles(() => ({
        formControl: {
            minWidth: 120,
        },
    }),
);
