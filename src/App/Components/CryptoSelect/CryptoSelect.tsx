import React from 'react';
import {connect} from "react-redux";
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import {getCryptocurrencyStateList} from "../../Redux/selector/selector";
import {addCryptocurrency} from "../../Redux/actions/cryptocurrency/cryptocurrencyActions";
import {useCryptoSelectStyles} from "./CryptoSelectStyles";
import {Cryptocurrency} from "../../Models/Cryptocurrency";

interface CryptoSelectProps {
    addItem: (item: number) => any;
    listItem: Cryptocurrency[];
}

const CryptoSelect: React.FC<CryptoSelectProps> = props => {

    const {addItem, listItem} = props;

    const classes = useCryptoSelectStyles();

    const changeHandler = event => addItem(event.target.value);

    return (
        <div>
            <FormControl className={classes.formControl}>
                <InputLabel htmlFor="age-native-simple">Cryptos</InputLabel>
                <Select
                    native
                    value={-1}
                    onChange={changeHandler}
                    inputProps={{
                        name: 'Cryptos',
                        id: 'cryptos-native-simple',
                    }}
                >
                    <option value=""/>
                    {
                        listItem.map( (item, i) =>
                            <option key={item.id} value={i}>{item.symbol}</option>
                        )
                    }
                </Select>
            </FormControl>
        </div>
    );
};

const mapStateToProps = state => ({listItem: getCryptocurrencyStateList(state)});
const mapDispatchToProps = dispatch => ({addItem: item =>  dispatch(addCryptocurrency(item))});

export default connect(mapStateToProps, mapDispatchToProps) (CryptoSelect);
