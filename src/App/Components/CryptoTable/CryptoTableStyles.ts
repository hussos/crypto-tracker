import {makeStyles} from "@material-ui/core/styles";

export const useCryptoTableStyles = makeStyles((theme) => ({
    root: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
        backgroundColor: '#424242'
    },
}));
