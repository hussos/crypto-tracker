import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import {removeCryptocurrency} from "../../Redux/actions/cryptocurrency/cryptocurrencyActions";
import {
    getAllCryptocurrency,
    getAllCryptocurrencyError,
    getCryptocurrencyStateTable
} from "../../Redux/selector/selector";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import CloseIcon from '@material-ui/icons/Close';
import Title from '../Title/Title';
import {useCryptoTableStyles} from "./CryptoTableStyles";
import {Grid, IconButton, Paper} from "@material-ui/core";
import CryptoSelect from "../CryptoSelect/CryptoSelect";
import EnhancedTableHead from "../EnhancedTableHead/EnhancedTableHead";
import {
    fetchCryptocurrency,
    fetchCryptocurrencyPrices
} from "../../Redux/actions-creator/cryptocurrencyActionCreator";
import {Cryptocurrency} from "../../Models/Cryptocurrency";
import {useSnackbar} from "notistack";

export type Order = 'asc' | 'desc';
export interface Data {
    rank: number;
    symbol: number;
    price: number;
}

interface CryptoTableProps {
    removeItem: (item: number) => any;
    populatePrices: (symbol: string) => any;
    populateItems: () => any;
    rows: Cryptocurrency[];
    allRows: Cryptocurrency[];
    error: {flag: boolean, message: string};
}

const CryptoTable: React.FC<CryptoTableProps> = props => {

    const {removeItem, populateItems, populatePrices, rows, allRows, error} = props;

    const [order, setOrder] = useState<Order>('asc');
    const [orderBy, setOrderBy] = useState<keyof Data>('rank');

    const classes = useCryptoTableStyles();

    const {enqueueSnackbar} = useSnackbar();

    useEffect(() => {
        populateItems();
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    useEffect(() => {
        let symbols: string[] = [];
        let symbolsList = '';

         if (error.message.length !== 0)  {
             enqueueSnackbar(error.message, {variant: (error.flag ? 'error' : 'success')});
        }

        if (allRows.length !== 0) {
            allRows.forEach( item => symbols.push(item.symbol));
            symbolsList = symbols.toString().replace(/,{1,}$/, '');
            populatePrices(symbolsList);
        }
    }, [rows, error]); // eslint-disable-line react-hooks/exhaustive-deps

    return (
        <Grid item xs={12}>
            <Paper
                classes={{
                    root: classes.root
                }}
            >
                <div style={{display: "flex", justifyContent: "space-between"}}>
                    <Title>Cryptocurrencies</Title>
                    <CryptoSelect />
                </div>
                <Table size="small">
                    <EnhancedTableHead
                        order={order}
                        orderBy={orderBy}
                        rows={rows}
                        setOrder={setOrder}
                        setOrderBy={setOrderBy}
                    />
                    <TableBody>
                        {
                            rows.map((row, index) => (
                                    <TableRow key={row.id}>
                                        <TableCell>{row.symbol}</TableCell>
                                        <TableCell>{row.rank}</TableCell>
                                        <TableCell>
                                            ${
                                                !!row && !!row.price && row.price.toFixed(4)
                                            }
                                        </TableCell>
                                        <TableCell>
                                            <IconButton onClick={() => removeItem(index)}>
                                                <CloseIcon/>
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                ))
                        }
                    </TableBody>
                </Table>
            </Paper>
        </Grid>
    );
};

const mapStateToProps = state => ({
    rows: getCryptocurrencyStateTable(state),
    allRows: getAllCryptocurrency(state),
    error: getAllCryptocurrencyError(state),
});

const mapDispatchToProps = dispatch => ({
    removeItem: item =>  dispatch(removeCryptocurrency(item)),
    populateItems: () => dispatch(fetchCryptocurrency()),
    populatePrices: symbols => dispatch(fetchCryptocurrencyPrices(symbols))
});

export default connect(mapStateToProps, mapDispatchToProps) (CryptoTable);
