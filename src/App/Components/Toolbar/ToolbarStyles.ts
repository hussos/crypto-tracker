import {makeStyles, Theme} from '@material-ui/core';

export const useMainToolbarStyles = makeStyles((theme: Theme) => ({
    grow: {
        flexGrow: 1,
        height: "65px"
    },
    title: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',
        },
    },
    appBarColor: {
        backgroundColor: "#0066d2"
    },
}));
