import React from 'react';
import {AppBar, Typography, Toolbar} from '@material-ui/core';
import {useMainToolbarStyles} from "./ToolbarStyles";


const MainToolbar: React.FC = () => {
    const classes = useMainToolbarStyles();

    return (
        <div className={classes.grow}>
            <AppBar
                classes={{
                    colorPrimary: classes.appBarColor
                }}
                position="fixed"
            >
                <Toolbar>
                    <Typography className={classes.title} variant="h6" noWrap>
                        Crypto Tracker
                    </Typography>
                </Toolbar>
            </AppBar>
        </div>
    );
}

export default MainToolbar;
