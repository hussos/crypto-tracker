export interface CryptocurrencyAsObject {
    id: number,
    rank: number,
    price: number,
    symbol: string
}

export class Cryptocurrency {
    private readonly _id: number;
    private readonly _rank: number;
    private readonly _symbol: string;
    private _price: number;

    constructor( id, rank, price, symbol ) {
        this._id = id;
        this._rank = rank;
        this._price = price;
        this._symbol = symbol;
    }

    public setPrice(price: number) {
        this._price = price;
    }

    get id(): number {
        return this._id;
    }

    get rank(): number {
        return this._rank;
    }

    get price(): number {
        return this._price;
    }

    get symbol(): string {
        return this._symbol;
    }

    static createFromJSONObject( {id, symbol, price, rank}: CryptocurrencyAsObject ) {
        return new Cryptocurrency( id, rank, price, symbol );
    }
}
