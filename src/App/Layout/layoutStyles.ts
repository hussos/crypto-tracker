import {makeStyles} from "@material-ui/core/styles";

export const useLayoutStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        backgroundColor: "#bababa"
    },
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    },
    appBarSpacer: theme.mixins.toolbar,
}));
