import React from 'react';
import CryptoTable from '../Components/CryptoTable/CryptoTable'
import MainToolbar from "../Components/Toolbar/Toolbar";
import {Container, Grid} from "@material-ui/core";
import {useLayoutStyles} from "./layoutStyles";


const Layout: React.FC = () => {
    const classes = useLayoutStyles();

    return (
        <div className={classes.root}>
            <main className={classes.content}>
                <div className={classes.appBarSpacer} />
                <MainToolbar />
                <Container maxWidth="lg" className={classes.container}>
                    <Grid container spacing={3}>
                        <CryptoTable />
                    </Grid>
                </Container>
            </main>
        </div>
    );
};

export default Layout;
