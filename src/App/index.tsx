import React from 'react';
import Layout from "./Layout";

const Index: React.FC = () => (<div className="App"><Layout /></div>);

export default Index;
