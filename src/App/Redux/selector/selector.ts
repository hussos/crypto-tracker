export const getCryptocurrencyStateTable = store => store.CryptocurrencyReducer.table;

export const getCryptocurrencyStateList = store => store.CryptocurrencyReducer.dropDown;

export const getAllCryptocurrency = store => store.CryptocurrencyReducer.all;

export const getAllCryptocurrencyError = store => store.CryptocurrencyReducer.error;

