import {ADD_CRYPTOCURRENCY, POPULATE_CRYPTOCURRENCY, REMOVE_CRYPTOCURRENCY, POPULATE_CRYPTOCURRENCY_PRICES} from "./cryptocurrencyActionTypes";

export const addCryptocurrency = content => ({
        type: ADD_CRYPTOCURRENCY,
        payload: {
            content
        }
});

export const removeCryptocurrency = content => ({
        type: REMOVE_CRYPTOCURRENCY,
        payload: {
            content
        }
});

export const populateCryptocurrency = content => ({
        type: POPULATE_CRYPTOCURRENCY,
        payload: {
            content
        }
});

export const populateCryptocurrencyPrices = content => ({
        type: POPULATE_CRYPTOCURRENCY_PRICES,
        payload: {
            content
        }
});
