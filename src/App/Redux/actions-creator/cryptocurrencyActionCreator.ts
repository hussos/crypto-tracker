import {populateCryptocurrency, populateCryptocurrencyPrices} from "../actions/cryptocurrency/cryptocurrencyActions";

export const fetchCryptocurrency = () => async dispatch =>
    fetch('https://www.stackadapt.com/coinmarketcap/map?limit=30')
        .then(response => response.json())
        .then(json => dispatch(populateCryptocurrency(json)))
        .catch( e => {
            console.error("error: ", e);
        });

export const fetchCryptocurrencyPrices = symbols => async dispatch =>
    fetch(`https://www.stackadapt.com/coinmarketcap/quotes?symbol=${symbols}`)
        .then(response => response.json())
        .then(json => {
            return dispatch(populateCryptocurrencyPrices(json))
        })
        .catch( e => {
            console.error("error: ", e);
        });
