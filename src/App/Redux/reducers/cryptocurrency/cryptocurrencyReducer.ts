import {
    ADD_CRYPTOCURRENCY,
    REMOVE_CRYPTOCURRENCY,
    POPULATE_CRYPTOCURRENCY,
    POPULATE_CRYPTOCURRENCY_PRICES
} from "../../actions/cryptocurrency/cryptocurrencyActionTypes";
import {Cryptocurrency} from "../../../Models/Cryptocurrency";

const initialState = {
    all: [],
    table: [],
    dropDown: [],
    error: {
        flag: false,
        message: ''
    }
};

/**
 *
 * @param state
 * @param action
 */
export default function (state = initialState, action) {
    switch (action.type) {
        case POPULATE_CRYPTOCURRENCY: {
            const { content } = action.payload;

            const allData: Cryptocurrency[] = content.data.map( data => Cryptocurrency.createFromJSONObject(data));
            const dropDownData: Cryptocurrency[] = allData.slice(5);
            const tableData: Cryptocurrency[] = allData.slice(0, 5);

            return {
                ...state,
                all: [...state.all, ...allData],
                dropDown: [...dropDownData],
                table: [...tableData]
            };
        }
        case POPULATE_CRYPTOCURRENCY_PRICES: {
            const { content } = action.payload;

            const allData: Cryptocurrency[] = [...state.all];

            allData.slice(0,30).map(all => all.setPrice(content.data[all.symbol].quote.USD.price));

            return {
                ...state,
                all: [...state.all, allData]
            };
        }
        case REMOVE_CRYPTOCURRENCY: {
            const { content } = action.payload;

            if (state.table.length > 1) {
                let tableList: Cryptocurrency[] = [...state.table];
                let item = tableList[content];
                let selectListItem: Cryptocurrency[] = tableList.splice(content, 1);

                return {
                    ...state,
                    table: tableList,
                    dropDown: [...state.dropDown, ...selectListItem],
                    error: {
                        flag: false,
                        message: `successfully removed ${item.symbol}`
                    }
                };
            } else return {
                ...state,
                error: {
                    flag: true,
                    message: "Error: can't have less than 1 item"
                }
            }
        }
        case ADD_CRYPTOCURRENCY: {
            const { content } = action.payload;

            if (state.table.length < 10) {

                let selectListItem: Cryptocurrency[] = [...state.dropDown];
                let item = selectListItem[content];
                let selectTable: Cryptocurrency[] = selectListItem.splice(content, 1);

                return {
                    ...state,
                    dropDown: selectListItem,
                    table: [...state.table, ...selectTable],
                    error: {
                        flag: false,
                        message: `successfully added ${item.symbol}`
                    }
                };
            } else return {
                ...state,
                error: {
                    flag: true,
                    message: "Error: can't track more than 10 items"
                }
            }
        }
        default:
            return state;
    }
}
